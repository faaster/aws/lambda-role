output "role" {
  description = "The API Gateway Lambda access role."
  value       = aws_iam_role.this
}
