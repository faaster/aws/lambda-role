
variable "name" {
  description = "The name for the role."
}

variable "path" {
  description = "The path for the role."
  default     = "/"
}

variable "enable_lambda_logging" {
  description = "Set this to `true` to enable Cloud Watch logging access for Lambda functions in this API. Disabled by default."
  default     = false
  type        = bool
}

variable "tags" {
  description = "Map of tags to attach to any resources in this module that support tags. Defaults to `{}`."
  type        = map(string)
  default     = {}
}
