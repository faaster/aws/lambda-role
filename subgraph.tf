
locals {
  dotname_role = "\"${var.name}_${md5(aws_iam_role.this.arn)}\""
}

output "graphviz" {
  description = "Description of the module instance for use with Graphviz `dot`. Nodes are named using `md5(arn)`."
  value = {
    subgraph = <<EOT
      subgraph { 
        node [shape=record]
        ${local.dotname_role} [label="APIGLambdaRole | ${var.name} | CW Logging ${var.enable_lambda_logging ? "Enabled" : "Disabled"} }"]
      }
    EOT
    nodes = {
      this = local.dotname_role
    }
  }
}
