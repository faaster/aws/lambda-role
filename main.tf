data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

resource "aws_iam_role" "this" {
  name        = "${var.name}APIGLambdaRole"
  path        = var.path
  description = "API Gateway Lambda Role, with logging to CloudWatch ${var.enable_lambda_logging ? "enabled" : "disabled"}."
  tags        = var.tags

  assume_role_policy = <<-EOF
    {
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
            "Service": [
                "lambda.amazonaws.com",
                "apigateway.amazonaws.com" ]
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
    EOF

}

# Enable CloudWatch logging if specified

data "aws_iam_policy_document" "this" {
  for_each = toset(var.enable_lambda_logging ? ["logging"] : [])
  statement {
    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:*",
    ]
  }
}

resource "aws_iam_role_policy" "this" {
  for_each = toset(var.enable_lambda_logging ? ["logging"] : [])
  name     = "${var.name}-lambda-cwlogs-access"
  role     = aws_iam_role.this.id
  policy   = data.aws_iam_policy_document.this[each.key].json
}

