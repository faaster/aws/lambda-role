# FaasTer/AWS API Resource Tree Module

> Module works; documentation is still a work in progress.

This is a Terraform module that can be used to define and create a standalone AWS Lambda Role. Even though this is a simple module, keeping it separate allows having one Lambda role for use with different Lambda functions.

- [Changelog](#changelog)
- [Using the Module](#using-the-module)
- [External References](#external-references)
- [Module Reference](#module-reference)
  - [Requirements](#requirements)
  - [Providers](#providers)
  - [Modules](#modules)
  - [Resources](#resources)
  - [Inputs](#inputs)
  - [Outputs](#outputs)

## Changelog

See the [repository tags list](https://gitlab.com/faaster/aws/lambda-role/-/tags) for the changelog.

## Using the Module

```hcl
module "my_lambda_role" {
  source = "git::https://gitlab.com/faaster/aws/lambda-role.git"

  name                  = "MY_Lambda_Role"
  enable_lambda_logging = true  # Attached CloudWatch access policy to the role
  tags = {
      environment = var.environment
  }
}
```

## External References

1. Terraform Best Practices | [Changelog Specification](https://www.terraform.io/docs/extend/best-practices/versioning.html#changelog-specification)

## Module Reference

### Requirements

| Name                                                                      | Version   |
| ------------------------------------------------------------------------- | --------- |
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.14   |
| <a name="requirement_aws"></a> [aws](#requirement\_aws)                   | >= 3.30.0 |

### Providers

| Name                                              | Version   |
| ------------------------------------------------- | --------- |
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 3.30.0 |

### Modules

No modules.

### Resources

| Name                                                                                                                               | Type        |
| ---------------------------------------------------------------------------------------------------------------------------------- | ----------- |
| [aws_iam_role.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role)                          | resource    |
| [aws_iam_role_policy.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy)            | resource    |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity)      | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_region.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/region)                        | data source |

### Inputs

| Name                                                                                                  | Description                                                                                                    | Type          | Default | Required |
| ----------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------- | ------------- | ------- | :------: |
| <a name="input_enable_lambda_logging"></a> [enable\_lambda\_logging](#input\_enable\_lambda\_logging) | Set this to `true` to enable Cloud Watch logging access for Lambda functions in this API. Disabled by default. | `bool`        | `false` |    no    |
| <a name="input_name"></a> [name](#input\_name)                                                        | The name for the role.                                                                                         | `any`         | n/a     |   yes    |
| <a name="input_path"></a> [path](#input\_path)                                                        | The path for the role.                                                                                         | `string`      | `"/"`   |    no    |
| <a name="input_tags"></a> [tags](#input\_tags)                                                        | Map of tags to attach to any resources in this module that support tags. Defaults to `{}`.                     | `map(string)` | `{}`    |    no    |

### Outputs

| Name                                                         | Description                                                                                       |
| ------------------------------------------------------------ | ------------------------------------------------------------------------------------------------- |
| <a name="output_graphviz"></a> [graphviz](#output\_graphviz) | Description of the module instance for use with Graphviz `dot`. Nodes are named using `md5(arn)`. |
| <a name="output_role"></a> [role](#output\_role)             | The API Gateway Lambda access role.                                                               |
